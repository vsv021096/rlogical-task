require('dotenv').config();

const express = require('express');
const app = express();
const bcrypt = require('bcrypt');
const bodyParser = require('body-parser');
const passport = require('passport');
const flash = require('express-flash');
const session = require('express-session');
const methodOverride = require('method-override');
const mongoose = require('mongoose');
const MongoDBStore = require('connect-mongodb-session')(session);
const initializePassport = require('./passport-config')
const users = require('./model/User');
var path = require('path');
const fileUpload = require('express-fileupload');
const sgMail = require('@sendgrid/mail');
import cron from 'node-cron';


app.use(fileUpload());
app.use(express.static(path.join(__dirname, 'public')));

initializePassport(
    passport,
    username => users.find(user => user.username === username),
    id => users.find(user => user.id === id)
)

const options = {
    useCreateIndex: true,
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: true
}

const store = new MongoDBStore({
    uri: process.env.MONGODB_URI,
    collection: 'sessions'
});

app.set('view-engine', 'ejs');
app.use(express.urlencoded({ extended: false }));
app.use(flash());
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    store: store
}));

app.use(passport.initialize())
app.use(passport.session())
app.use(methodOverride('_method'))

app.post('/login', checkNotAuthenticated, passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
}));

//register route to save and update the user data.
app.put('/register', checkNotAuthenticated, async (req, res) => {
    try {
        if (req.files.profile_pic.mimetype == 'image/jpeg' || req.files.profile_pic.mimetype == 'image/jpg' || req.files.profile_pic.mimetype == 'image/png'){
            if (req.body.password !== req.body.conf_password){
                return res.status(401).json({'status': 'failed', 'message': 'Password and confirm password must be same.'});
            }
            const user_id = req.body.user_id;
            if (user_id){
                registerUser.findById(user_id)
                .then(userData => {
                    userData.username = req.body.username,
                    userData.password = hashedPassword,
                    userData.name = req.body.name,
                    userData.email = req.body.email,
                    userData.profile_pic_name = req.files.profile_pic.name,
                    userData.contact_no = req.body.contact_no,
                    userData.profile_pic = req.files.profile_pic.data,
                    userData.country = req.body.country
                    return userData.save();
                })
                .then(result => {
                    return res.status(201).json({ 'status': 'success', 'message': 'User information updated Successfully.' });
                })
                .catch(err => {
                    console.log(err);
                    return res.status(401).json({ 'status': 'failed', 'message': 'Problem in updating user information. Please try again.' });
                });
            } else {
                const hashedPassword = await bcrypt.hash(req.body.password, 10);
                const registerUser = new users({
                    username : req.body.username,
                    password : hashedPassword,
                    name : req.body.name,
                    email : req.body.email,
                    profile_pic_name : req.files.profile_pic.name,
                    contact_no : req.body.contact_no,
                    profile_pic : req.files.profile_pic.data,
                    country : req.body.country
                });
                registerUser.save()
                    .then(result => {
                        return res.status(201).json({ 'status': 'success', 'message': 'User added Successfully.' })
                    })
                    .catch(err => {
                        console.log(err);
                        return res.status(401).json({ 'status': 'failed', 'message': 'Problem in adding user. Please try again.' })
                    })
            }
        } else {
            return res.status(401).json({'status': 'failed', 'message': 'Invalid File uploaded.'});
        }
    } catch {
        return res.status(500).json({'status': 'failed', 'message': 'Internal server error.'});
    }
})

app.delete('/logout', (req, res) => {
    req.logOut()
})

app.get('/user_data', (req, res) => {
    registerUser
        .find()
        .then(result => {
            return res.status(200).json(result);
        })
        .catch(err => {
            console.log(err);
        })
})


function checkAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return next()
    }
    return res.status(401).json({'status': 'failed', 'message': 'User is not authenticated.'});
}

function checkNotAuthenticated(req, res, next) {
    if (req.isAuthenticated()) {
        return res.status(401).json({'status': 'failed', 'message': 'User is not authenticated.'});
    }
    next()
}

const sendgrid_scheduler = () => {
    sgMail.setApiKey(process.env.SENDGRID_API_KEY)
    const msg = {
        to: 'vsv021096@gmail.com', // Change to your recipient
        from: 'admin@bluewinez.com', // Change to your verified sender
        subject: 'Sending with SendGrid is Fun',
        text: 'and easy to do anywhere, even with Node.js',
        html: '<strong>and easy to do anywhere, even with Node.js</strong>',
    }
    sgMail
    .send(msg)
    .then(() => {
        console.log('Email sent')
    })
    .catch((error) => {
        console.error(error)
    })
};

cron.schedule('0 */30 * * * *' , sendgrid_scheduler);

mongoose
    .connect(
        process.env.MONGODB_URI,
        options
    )
    .then(result => {
        app.listen(3001);
    })
    .catch(err => {
        console.log(err);
    });
